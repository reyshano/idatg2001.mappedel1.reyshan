package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * DepartmentTest class where the remove method is tested.
 */
class DepartmentTest {

    /**
     * The Hospital.
     */
    Hospital hospital;

    /**
     * The Department.
     */
    Department department;

    /**
     * The Employee to remove.
     */
    Employee employeeToRemove;

    /**
     * The Patient to remove.
     */
    Patient patientToRemove;


    /**
     * Setting up hospital object and existing employee and patient to remove.
     */
    @BeforeEach
    void setUp() {
        hospital = new Hospital("St. Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        department = hospital.getDepartments().get(0);
        employeeToRemove = hospital.getDepartments().get(0).getEmployees().get(0);
        patientToRemove = hospital.getDepartments().get(0).getPatients().get(0);

    }


    /**
     * Test removing an existing employee from the employee register.
     */
    @DisplayName("Removing an existing employee from the employee register")
    @Test
    void testRemovingExistingEmployee() {

        int registerSizeBeforeRemoval = department.getEmployees().size();

        try {

            department.remove(employeeToRemove);

        } catch (RemoveException removeException) {
            System.err.println("The person could not be removed. More info: " + removeException.getMessage());

        }
        int registerSizeAfterRemoval = department.getEmployees().size();

        // checks if the size of the employee register is the same before and after removing an employee
        assertEquals(registerSizeBeforeRemoval - 1, registerSizeAfterRemoval);

    }

    /**
     * Testing removing an existing patient from the patient register.
     */
    @DisplayName("Removing an existing patient from the patient register")
    @Test
    void testRemovingExistingPatient() {

        int registerSizeBeforeRemoval = department.getPatients().size();

        try {

            department.remove(patientToRemove);

        } catch (RemoveException removeException) {
            System.err.println("The person could not be removed. More info: " + removeException.getMessage());

        }
        int registerSizeAfterRemoval = department.getPatients().size();

        // checks if the size of the patient register is the same before and after removing a patient
        assertEquals(registerSizeBeforeRemoval - 1, registerSizeAfterRemoval);

    }

    /**
     * Testing removing a non existing employee from the employee register.
     */
    @DisplayName("Removing a non existing employee from the employee register")
    @Test
    void testRemoveNonExistingEmployee() {
        Employee removeNonExistingEmployee = new Employee ("NonExisting", "Employee", "5555");

        assertThrows(RemoveException.class, () -> department.remove(removeNonExistingEmployee));

    }


    /**
     * Testing removing a non existing patient from the patient register.
     */
    @DisplayName("Removing a non existing patient from the patient register")
    @Test
    void testRemoveNonExistingPatient() {
        Patient removeNonExistingPatient = new Patient ("NonExisting", "Employee", "5555");

        assertThrows(RemoveException.class, () -> department.remove(removeNonExistingPatient));

    }


    /**
     * Testing removing a null object.
     */
    @DisplayName("Removing null object")
    @Test
    void testRemoveNullObject(){

        assertThrows(RemoveException.class, () -> department.remove(null));
    }

}