package mappe.del1.hospital;

/**
 *  An abstract class Person that contains relevant information about a person.
 */
public abstract class Person {

    /**
     * The fields of the class with the first name, last name and social security number of a person.
     */
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * This method gets the social security number of a person.
     * @return the social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * This method sets the social security number of a person.
     * @param socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * This method gets the first name of a person.
     * @return the first name of the person
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * This method sets the first name of a person.
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * This method gets the last name of a person.
     * @return the last name of the person
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * This method sets the last name of a person.
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * This method gets the full (first and last) name of a person.
     * @return full name as a String
     */
    public String getFullName() {
        return (this.firstName + " " +  this.lastName);
    }

    /**
     * The constructor of the class where an instance of a new person is created.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }


    @Override
    public String toString() {
        return "Person{" +
                "Name='" + getFullName() + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}
