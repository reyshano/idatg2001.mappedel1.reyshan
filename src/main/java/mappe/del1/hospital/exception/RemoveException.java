package mappe.del1.hospital.exception;


/**
 * This RemoveException class is used to send an error message to the user when en error occurs.
 */
public class RemoveException extends Exception {

    private static final long serialVersionUID = 1L;


    /**
     * @param exceptionMessage
     */
    public RemoveException(String exceptionMessage) {
        super(exceptionMessage);
    }
}

