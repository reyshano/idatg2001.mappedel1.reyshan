package mappe.del1.hospital;

/**
 * The Patient class extends the Person class and implements the Diagnosable interface.
 * This class contains all relevant information about a patient.
 */
public class Patient extends Person implements Diagnosable {

    private String diagnosis = "";


    /**
     * This method sets the diagnosis of a patient.
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * This method gets the diagnosis of a patient.
     * @return patient diagnosis
     */
    protected String getDiagnose() {
        return diagnosis;
    }

    /**
     * The constructor where an instance of a new patient is created.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }


    @Override
    public String toString() {
        return super.toString();
    }
}
