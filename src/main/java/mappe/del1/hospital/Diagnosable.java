package mappe.del1.hospital;

/**
 * The interface Diagnosable
 */
public interface Diagnosable {

    /**
     * This method sets the diagnosis of a patient.
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis);


    }
