package mappe.del1.hospital;

/**
 * The Employee class extends the class Person.
 * This class contains all of an employee's, that works at the hospital, relevant data.
 */
public class Employee extends Person {

    /**
     * The constructor where an instance of a new employee.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
