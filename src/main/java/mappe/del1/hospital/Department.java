package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.*;

/**
 * This Department class contains information about
 * all the departments, employees and patients in the hospital.
 */
public class Department {

    private String departmentName;
    public List<Employee> Employee;
    public List<Patient> Patient;


    /**
     * This method sets the name of a new department and
     * creates one ArrayList for employees and one ArrayList for patients.
     * @param departmentName
     */
    public Department(String departmentName) {
        setDepartmentName(departmentName);

        Employee = new ArrayList<>();
        Patient = new ArrayList<>();
    }

    /**
     * This method gets the name of a department in the hospital.
     * @return name of the department
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * This method sets the name of a department in the hospital.
     * @param departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * This method returns a list of all the employees in the hospital.
     * @return list with employees
     */

    public List<Employee> getEmployees() {
        return Employee;
    }

    /**
     * This method returns a list of all the patients in the hospital.
     * @return list with patients
     */
    public List<Patient> getPatients() {
        return Patient;
    }

    /**
     * This method adds a new employee to the Employees ArrayList.
     * @param newEmployee
     */
    public void addEmployee(Employee newEmployee) {
        Employee.add(newEmployee);
    }

    /**
     * This method adds a new employee to the Patients ArrayList.
     * @param newPatient
     */
    public void addPatient(Patient newPatient) {
        Patient.add(newPatient);
    }


    /**
     * This method removes an object of the type Patient or Employee.
     * Throws RemoveException if the patient or employee doesn't exist in their corresponding registers.
     * @param removePerson
     * @throws RemoveException
     */
    public void remove (Person removePerson) throws RemoveException {

        if (removePerson instanceof Employee) {
            if (Employee.contains(removePerson)) {
                Employee.remove(removePerson);
            }
            else {
                throw new RemoveException("This employee does not exist in the employee register.");
            }
        }

        else if (removePerson instanceof Patient) {
            if (Patient.contains(removePerson)) {
                Patient.remove(removePerson);
            }
            else {
                throw new RemoveException("This patient does not exist in the patient register.");
            }
        }

        else {
            throw new RemoveException("Person provided is not of type Employee or Patient");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return getDepartmentName().equals(that.getDepartmentName()) &&
                Employee.equals(that.Employee) &&
                Patient.equals(that.Patient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName(), Employee, Patient);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", Employee=" + Employee +
                ", Patient=" + Patient +
                '}';
    }
}
