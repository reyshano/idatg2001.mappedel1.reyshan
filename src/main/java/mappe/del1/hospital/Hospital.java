package mappe.del1.hospital;

import java.util.ArrayList;

/**
 * The class Hospital contains an ArrayList of all the departments in the hospital.
 */
public class Hospital {

    private final String hospitalName;
    private ArrayList<Department> departments;


    /**
     * This method sets the name of the hospital and creates the ArrayList departments.
     * @param hospitalName
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    /**
     * This method gets the name of the hospital.
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * This method returns an array list containing all the departments in the hospital.
     * @return the ArrayList departments
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * This method first checks if the inputted department already exits,
     * and if not adds the new department to the array list with all the departments.
     * @param newDepartment
     */
    public void addDepartments(Department newDepartment) {
        if(!departments.contains(newDepartment)) {
            this.departments.add(newDepartment);
        }
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
