package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 * This Surgeon class extends the class Doctor.
 */
public class Surgeon extends Doctor {

    /**
     * This constructor instantiates a new Surgeon.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */

    // According to the class diagram in the project description the constructor of this class
    // is supposed to be protected, but because of my packaging structure that is not possible.
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * This method sets the diagnosis of a given patient.
     * @param patient
     * @param diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
