package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Employee;
import mappe.del1.hospital.Patient;

/**
 * This Doctor class extends the class Employee.
 */
public abstract class Doctor extends Employee {

    /**
     * This protected constructor instantiates a new Doctor.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }


    /**
     * This is an abstract method that allows the subclasses
     * of this class to set a diagnosis for a given patient.
     * @param patient
     * @param diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
