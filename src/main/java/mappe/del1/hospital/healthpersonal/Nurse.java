package mappe.del1.hospital.healthpersonal;

import mappe.del1.hospital.Employee;

/**
 * This Nurse class extends the class Employee.
 * This class contains all of a nurse's relevant data.
 */
public class Nurse extends Employee {

    /**
     * This constructor instantiates a new Nurse.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Nurse:\n" + super.toString();
    }
}
