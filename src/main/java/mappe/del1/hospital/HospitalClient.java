package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;


/**
 * The class HospitalClient.
 */
public class HospitalClient {

    /**
     * The entry point of the application.
     * @param args
     */
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St. Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        Department department = hospital.getDepartments().get(0);

        //Existing employee to remove
        Employee employeeToRemove = hospital.getDepartments().get(0).getEmployees().get(0);

        // Creating non existent patient to remove
        Patient patientToRemove = new Patient("Reyshan", "Osmanova", "6423");

        try {

            //Should succeed
            System.out.println("Employee to be removed: " + employeeToRemove);
            department.remove(employeeToRemove);
            System.out.println("Employee " + employeeToRemove.getFullName() + " is removed.");



            //Should fail since the patient doesn't exist
            System.out.println("Patient to be removed: " + patientToRemove);
            department.remove(patientToRemove);
            System.out.println("Patient " + patientToRemove.getFullName() + " is removed.");


        } catch (RemoveException removeException) {
            System.err.println("The person could not be removed. More info: " + removeException.getMessage());
        }
    }

}

